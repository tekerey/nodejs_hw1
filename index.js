const http = require('http');
const fs = require('fs');
const url = require('url');

// should we use sync versions of these functions???
function log(message, time) {
    fs.readFile('logs.json', (err, data) => {
        let obj;
        if (err) {
            obj = {logs: [{message: message, time: time}]};
        }
        else {
            obj = JSON.parse(data);
            obj.logs.push({message: message, time: time});
        }

        fs.writeFile('logs.json', JSON.stringify(obj),
            (err) => {
                if (err) console.log(`Error while trying to log '${message}':${time}`);
            }
        );
    });
}

module.exports = () => {
    http.createServer((req, res) => {
        const { query, pathname } = url.parse(req.url, true);

        // save file
        if (req.method === 'POST' && pathname === '/file') {
            fs.access('./files/', (err) => {
                if (err) fs.mkdirSync('files');
                if (!query.filename || !query.content) {
                    res.writeHead(400, {'Content-Type': 'text/html'});
                    res.end();
                }
                fs.writeFile('./files/' + query.filename, query.content, (err) => {
                    if (err) {
                        res.writeHead(400, {'Content-Type': 'text/html'});
                        res.end();
                    } else {
                        res.writeHead(200, {'Content-Type': 'text/html'});
                        res.end(`New file with name '${query.filename}' saved.`);
                        log(`New file with name '${query.filename}' saved`,
                            Date.now()
                        );
                    }
                });
            });
            return;
        }

        // get file
        if (req.method === 'GET' && /^\/file\//.test(pathname)) {
            fs.readFile('./files/' + pathname.split('/')[2], (err, data) => {
                if (err) {
                    res.writeHead(400, {'Content-Type': 'text/html'});
                    res.end();
                } else {
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.end(data);
                    log(`File '${pathname.split('/')[2]}' was requested`,
                        Date.now()
                    );
                }
            });
            return;
        }

        // get logs
        if (req.method === 'GET' && pathname === '/logs') {
            fs.readFile('logs.json', (err, data) => {
                if (err) {
                    res.writeHead(400, {'Content-Type': 'text/html'});
                    res.end();
                } else {
                    let obj = JSON.parse(data);
                    obj.logs.sort((a,b) => a.time - b.time);
                    const from = query.from || obj.logs[0].time;
                    const to = query.to || obj.logs[obj.logs.length-1].time;
                    obj.logs = obj.logs.filter(el => el.time >= from &&
                        el.time <= to);
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.end(JSON.stringify(obj));
                }
            });
            return;
        }

        res.writeHead(404, {'Content-Type': 'text/html'});
        res.end();
    }).listen(process.env.PORT || 8080);
}